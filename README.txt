functionThis a project based on the "Boss Challenge Level 3" from the Udemy course
"The Complete 2020 Web Development Bootcamp".

  Goal: create a fully fledged website (front-end and back-end) from scratch

I tried to use as little of the tutorial and/or hints as I could, however the end product should still look very similar to the webpage in the challenge.

The CSS stylesheet only contained the styles for the navbar, the footer, and the container size/spacing. The basic structure of the whole project as well as the npm JSON package were provided, as well. The header and footer format were also provided, but no CSS styles we added. In the app.js file, the requirements, calling express(), the listening port, and the starting content of the Home page, Contact Us page and About Us page were already written. However, no GET or POST functions were pre-written, nor were any variables I would need to use within it. None of the files for the webpages themselves contained any content.

9/29/2020

  3:18 pm:
    I looked at the tutorial to see what the routing parameter in the blog post GET function was
    called. I did not know the name of it before hand, and I have been having trouble figuring out
    how to get the special characters to be included in the check with the blog title. I determined
    that it would be faster to listen to what she has to say then it would be to figure it out
    without following the lesson. I have the resource there. There is no reason not to use it.

  7:05 pm:
    I could not get the blog post link with special characters to work for a little while even using lodash for some reason. I kept getting an error like this:

      Error [ERR_HTTP_HEADERS_SENT]: Cannot set headers after they are sent to the client

    I read online that it said this meant that you were trying to send information to the client after you had already done so. There were no commands written after my call to render() (just a closing bracket and the rest of an if/else statement), so I don't really know what was causing the error. I commented out the render function and replaced it with a normal res.send() request, and it worked fine. Then, I uncommented render() after that and moved it below send() (which I commented out to use render()), and it worked just fine. I'm not really sure what happened here, but it works now.
      - Never mind. It still didn't work with '?'

  7:47 pm
    I ended up rewriting the GET function for the blog entry page with a forEach loop, and for some reason, it works as it should. I am not entirely sure what the difference is between a regular for-loop and a forEach loop, but it seems to make a difference.

  8:20 pm
    I looked at the teacher's version of the stylesheet and adjusted mine to match her's since mine wasn't working properly. I understood why min-height, but why it was necessary to set the positioning to relative. Why was it necessary to do this?

    HTML element set to static positioning by default, so even though elements were dynamically being added to page, the size of the container wasn't getting bigger (or at least, that's what it seemed like). The footer's positioning was absolute, and the 'bottom' was set to 0, so it was looking for the bottom of its parent's container. This is why I assume the container wasn't actually getting bigger hence why it did not move even after elements were added.

    In order to fix this, it was necessary to set the whole HTML page to have relative positioning so that the container would expand as more elements were added.
